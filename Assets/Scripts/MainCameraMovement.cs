﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainCameraMovement : MonoBehaviour {

    public Image image;
    string currentPos = "Home";

    IEnumerator FadeOutToPosition(Vector3 position, Vector3 cameraAngle)
    {
        image.CrossFadeAlpha(1, .5f, false);
        yield return new WaitForSeconds(0.5f);
        image.CrossFadeAlpha(0, .5f, false);
        transform.position = position;
        transform.eulerAngles = cameraAngle;
    }

    // Use this for initialization
    void Start () {

        image.CrossFadeAlpha(0, 0f, false);

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            moveToHome();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            moveToHomeLoans();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            moveToEverydayBanking();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            moveToBusinessBanking();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            moveToInvestments();
        }

    }

    void moveToHome ()
    {
        StartCoroutine(FadeOutToPosition(new Vector3(227, -507, 0), new Vector3(0, -354, 0)));        
    }

    void moveToHomeLoans()
    {
        StartCoroutine(FadeOutToPosition(new Vector3(1085, -507, 1026), new Vector3(0, 57, 0)));
    }

    void moveToEverydayBanking()
    {
        StartCoroutine(FadeOutToPosition(new Vector3(1178, -507, -785), new Vector3(0, 138, 0)));
    }

    void moveToBusinessBanking()
    {
        StartCoroutine(FadeOutToPosition(new Vector3(-721, -507, 1321), new Vector3(0, -428, 0)));
    }

    void moveToInvestments()
    {
        StartCoroutine(FadeOutToPosition(new Vector3(-1670, -507, -863), new Vector3(0, -305, 0)));
    }

}
