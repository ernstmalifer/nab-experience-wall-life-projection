﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Transform stars;

    // Use this for initialization
    void Start () {

         iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("SwitchScene"), "time", 10, "easetype", iTween.EaseType.linear, "loopType", "loop"));

    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0,0,-36) * Time.deltaTime);
        stars.transform.rotation = transform.rotation;
    }
}
