﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class MainCameraMovementAuto : MonoBehaviour
{

    public Image image;
    String currentPos = "Home";

    IEnumerator FadeOutToPosition(Vector3 position, Vector3 cameraAngle)
    {
        Debug.Log("Fade To Position");
        image.CrossFadeAlpha(1, .5f, false);
        yield return new WaitForSeconds(0.5f);
        image.CrossFadeAlpha(0, .5f, false);
        transform.position = position;
        transform.eulerAngles = cameraAngle;
        Debug.Log("position: " + position.ToString());
        Debug.Log("angle: " + cameraAngle.ToString());
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("Started");
        InvokeRepeating("AutoMove", 0, 200f);
        //InvokeRepeating("AutoTeleport", 0, 35f);
        image.CrossFadeAlpha(0, 0f, false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            moveToHomeTeleport();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            moveToHomeLoansTeleport();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            moveToEverydayBankingTeleport();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            moveToBusinessBankingTeleport();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            moveToInvestmentsTeleport();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            moveToPhotosphereTeleport();
        }

        if (currentPos == "Home")
        {
            transform.Rotate(Vector3.up * Time.deltaTime * 20);
        }
    }

    void AutoMove()
    {

        Invoke("moveToHomeLoans", 10);
        Invoke("moveToHomeLoansToHome", 30);
        Invoke("moveToEverydayBanking", 50);
        Invoke("moveToEverydayBankingToHome", 70);
        Invoke("moveToBusinessBanking", 90);
        Invoke("moveToBusinessBankingToHome", 110);
        Invoke("moveToInvestments", 130);
        Invoke("moveToInvestmentsToHome", 150);
        Invoke("moveToPhotosphere", 170);
        Invoke("moveToPhotosphereToHome", 180);
    }

    void AutoTeleport()
    {
        Debug.Log("Auto Teleport Started");
        Invoke("moveToHomeTeleport", 5);
        Invoke("moveToHomeLoansTeleport", 10);
        Invoke("moveToEverydayBankingTeleport", 15);
        Invoke("moveToBusinessBankingTeleport", 20);
        Invoke("moveToInvestmentsTeleport", 25);
        Invoke("moveToPhotosphereTeleport", 30);

    }

    void lookTo()
    {
        Debug.Log(currentPos.ToString());
        switch (currentPos)
        {
            case "Home":
                iTween.RotateTo(gameObject, new Vector3(0, 0, 0), 1);
                break;
            case "HomeLoans":
                iTween.RotateTo(gameObject, new Vector3(0, 56, 0), 1);
                break;
            case "EverydayBanking":
                iTween.RotateTo(gameObject, new Vector3(0, 135, 0), 1);
                break;
            case "BusinessBanking":
                iTween.RotateTo(gameObject, new Vector3(0, 239, 0), 1);
                break;
            case "Investments":
                iTween.RotateTo(gameObject, new Vector3(0, 226, 0), 1);
                break;
            case "Photosphere":
                iTween.RotateTo(gameObject, new Vector3(332.7f, 86.8f, 0), 1);
                break;
            default:
                break;
        }
    }

    void moveToHomeLoans()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("HomeLoans"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "HomeLoans";

        //iTween.LookTo(gameObject, new Vector3(0, 56, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 56, 1), 0);
    }

    void moveToHomeLoansToHome()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPathReversed("HomeLoans"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Home";

        //iTween.LookTo(gameObject, new Vector3(0, 0, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 1), 0);
    }

    void moveToEverydayBanking()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("EverydayBanking"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "EverydayBanking";

        //iTween.LookTo(gameObject, new Vector3(0, 135, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 135, 1), 0);
    }

    void moveToEverydayBankingToHome()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPathReversed("EverydayBanking"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Home";

        //iTween.LookTo(gameObject, new Vector3(0, 0, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 1), 0);
    }

    void moveToBusinessBanking()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("BusinessBanking"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "BusinessBanking";

        //iTween.LookUpdate(gameObject, new Vector3(0, 239, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 239), 0);
    }

    void moveToBusinessBankingToHome()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPathReversed("BusinessBanking"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Home";

        //iTween.LookUpdate(gameObject, new Vector3(0, 0, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 1), 0);
    }

    void moveToInvestments()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Investments"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Investments";

        //iTween.LookUpdate(gameObject, new Vector3(0, 226, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 226, 1), 0);
    }

    void moveToInvestmentsToHome()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPathReversed("Investments"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Home";

        //iTween.LookUpdate(gameObject, new Vector3(0, 0, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 1), 0);
    }

    void moveToPhotosphere()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("Photosphere"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Photosphere";

        //iTween.LookUpdate(gameObject, new Vector3(0, 226, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 226, 1), 0);
    }

    void moveToPhotosphereToHome()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPathReversed("Photosphere"), "time", 10, "easetype", iTween.EaseType.linear, "orientToPath", true, "oncomplete", "lookTo", "oncompletetarget", gameObject));
        currentPos = "Home";

        //iTween.LookUpdate(gameObject, new Vector3(0, 0, 0), 1);
        //transform.eulerAngles = new Vector3(0, Mathf.LerpAngle(transform.eulerAngles.y, 0, 1), 0);
    }

    // Static Calls
    void moveToHomeTeleport()
    {
        Debug.Log("moveToHomeTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(0, -3, 0), new Vector3(0, 0, 0)));
    }

    void moveToHomeLoansTeleport()
    {
        Debug.Log("moveToHomeLoansTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(11.65446f, -3, 9.067366f), new Vector3(0, 56, 0)));
    }

    void moveToEverydayBankingTeleport()
    {
        Debug.Log("moveToEverydayBankingTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(14.11743f, -3, -11.99783f), new Vector3(0, 135, 0)));
    }

    void moveToBusinessBankingTeleport()
    {
        Debug.Log("moveToBusinessBankingTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(-13.52575f, -3, 11.0966f), new Vector3(0, 239, 0)));
    }

    void moveToInvestmentsTeleport()
    {
        Debug.Log("moveToInvestmentsTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(-14.99589f, -3, -11.7421f), new Vector3(0, 226, 0)));
    }

    void moveToPhotosphereTeleport()
    {
        Debug.Log("moveToPhotosphereTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(22, -3, -1.205699f), new Vector3(332.7f, 86.8f, 0)));
    }


}