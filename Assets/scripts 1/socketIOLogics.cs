﻿using SocketIO;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class socketIOLogocs : MonoBehaviour {

    private SocketIOComponent socket;

    public Image image;
    string currentPos = "Home";

    IEnumerator FadeOutToPosition(Vector3 position, Vector3 cameraAngle)
    {
        Debug.Log("Fade To Position");
        image.CrossFadeAlpha(1, .5f, false);
        yield return new WaitForSeconds(0.5f);
        image.CrossFadeAlpha(0, .5f, false);
        transform.position = position;
        transform.eulerAngles = cameraAngle;
        Debug.Log("position: " + position.ToString());
        Debug.Log("angle: " + cameraAngle.ToString());
    }

    // Use this for initialization
    void Start () {
        GameObject go = GameObject.Find("SocketIO");
        socket = go.GetComponent<SocketIOComponent>();

        StartCoroutine("CalltoServer");

        socket.On("fetching", (SocketIOEvent e) =>
        {
            var message = e.data.GetField("message").ToString();
        });

    }
	
	// Update is called once per frame
	void Update () {

    }

    // Static Calls
    void moveToHomeTeleport()
    {
        Debug.Log("moveToHomeTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(1, -30, 5.75f), new Vector3(0, 0, 0)));
    }

    void moveToHomeLoansTeleport()
    {
        Debug.Log("moveToHomeLoansTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(111.9956f, -30, 88.11318f), new Vector3(0, 56, 0)));
    }

    void moveToEverydayBankingTeleport()
    {
        Debug.Log("moveToEverydayBankingTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(137, -30, -105), new Vector3(0, 135, 0)));
    }

    void moveToBusinessBankingTeleport()
    {
        Debug.Log("moveToBusinessBankingTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(-130, -30, 108), new Vector3(0, 239, 0)));
    }

    void moveToInvestmentsTeleport()
    {
        Debug.Log("moveToInvestmentsTeleport");
        StartCoroutine(FadeOutToPosition(new Vector3(-146, -30, -108), new Vector3(0, 226, 0)));
    }
}
