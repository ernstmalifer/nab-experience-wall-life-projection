﻿using UnityEngine;
using System.Collections;

public class waypoint : MonoBehaviour {
	public GameObject[] outconnections;
	int selector, length;
	// Use this for initialization
	void Start () {
		length = outconnections.Length;
		this.GetComponent<MeshRenderer> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}

	void OnTriggerEnter ( Collider objectCollide ) {
		if (length > 0) {
			if (objectCollide.gameObject.tag == "car") {
			


				selector = Random.Range (0, length);
				objectCollide.gameObject.SendMessage ("moveNext", outconnections [selector]);

			}
		} else {

		
			objectCollide.gameObject.SendMessage ("noTarget");
		}
	}
}
