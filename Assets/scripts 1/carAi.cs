﻿using UnityEngine;
using System.Collections;

public class carAi : MonoBehaviour {
	public float carSpeed;
	public float rotateSpeed;
	private float distance;
	public float marginHigh,marginMid,marginLow;
	public float speedHigh, speedMid, speedLow;
	public Transform nextMove;
	public float collisionDetect;
	private Vector3 target;
	bool move;
	void Start(){
		
	}


	void moveNext ( GameObject checkpoint ) {
		nextMove = checkpoint.gameObject.transform;

	}
	void noTarget (){

		move = false;


	}

	void Update(){
		RaycastHit hit;
		move=true;
		if (Physics.Raycast (this.transform.position, transform.forward, out hit, collisionDetect)) {
			
			if (hit.transform.gameObject.tag == "car") {
				move = false;
			} 
		} 

		//transform.LookAt (nextMove);
		if (move) {
			distance = Vector3.Distance (this.transform.position, nextMove.transform.position);

			if (distance > marginHigh) {
				carSpeed = speedHigh;
			}else if (distance > marginMid) {
				carSpeed = speedMid;
			} else 	if (distance > marginLow) {
				carSpeed =speedLow;
			}

			
			Quaternion targetRotation = Quaternion.LookRotation (nextMove.transform.position - transform.position);

			// Smoothly rotate towards the target point.
			transform.rotation = Quaternion.Slerp (transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
			this.GetComponent<Rigidbody> ().AddForce (this.transform.forward * carSpeed);
	
		}


	}
}
